from django.shortcuts import render, redirect
import openai
from django.views import View


# Create your views here.

class HomeView(View):

    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        context = {}

        # set the model
        model_engine = "text-davinci-002"

        # authenticate with API key
        openai.api_key = "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"


        if request.method == "POST":
            Qprompt = request.POST.get('prompt')

            # generate a response
            completion = openai.Completion.create(
                engine=model_engine,
                prompt=Qprompt,
                max_tokens=1024,
                n=1,
                stop=None,
                temperature=0.5,
                # model="text-davinci-002",
                frequency_penalty=0,
                presence_penalty=0
            )

            chatResponse = completion.choices[0].text

            context = {
                'chatResponse': chatResponse,
                'prompt': Qprompt
            }

            return render(request, 'index.html', context)
        else:
            context["raise_error"] = "Something went wrong.. ..."
        redirect("/")
